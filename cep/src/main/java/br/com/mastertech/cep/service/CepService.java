package br.com.mastertech.cep.service;

import br.com.mastertech.cep.client.Cep;
import br.com.mastertech.cep.client.CepClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepClient cepClient;

    public Cep pegarCep(String cep)
    {
        return cepClient.getCep(cep);
    }

}
