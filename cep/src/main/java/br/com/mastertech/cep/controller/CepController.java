package br.com.mastertech.cep.controller;

import br.com.mastertech.cep.client.Cep;
import br.com.mastertech.cep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class CepController {

    @Autowired
    private CepService cepService;

    @GetMapping("/{cep}")
    public Cep pegarCep(@PathVariable String cep)
    {
        return cepService.pegarCep(cep);
    }

}
