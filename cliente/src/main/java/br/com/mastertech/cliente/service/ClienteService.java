package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.client.Cep;
import br.com.mastertech.cliente.client.CepClient;
import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CepClient cepClient;

    public Cliente criarCliente(String nome, String cep) {
        Cliente cliente = new Cliente();
        cliente.setId(0);
        cliente.setNome(nome);

        Cep cepApi = cepClient.getCep(cep);

        cliente.setBairro(cepApi.getBairro());
        cliente.setCep(cepApi.getCep());
        cliente.setCidade(cepApi.getCidade());
        cliente.setLogradouro(cepApi.getLogradouro());

        return clienteRepository.save(cliente);
    }

    public Cliente retornarClientePorId(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if(optionalCliente.isPresent())
            return optionalCliente.get();
        throw new RuntimeException("Cliente não existe!");
    }

}
