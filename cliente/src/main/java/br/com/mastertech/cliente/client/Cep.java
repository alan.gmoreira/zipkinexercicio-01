package br.com.mastertech.cliente.client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cep {

    private String logradouro;

    private String bairro;

    @JsonProperty("localidade")
    private String cidade;

    private String cep;

    public Cep() {
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
