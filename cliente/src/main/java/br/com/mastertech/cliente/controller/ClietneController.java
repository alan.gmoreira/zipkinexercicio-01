package br.com.mastertech.cliente.controller;

import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clienteCep")
public class ClietneController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping("/{nome}/{cep}")
    public Cliente criarCliente(@PathVariable String nome, @PathVariable String cep)
    {
        return clienteService.criarCliente(nome, cep);
    }

    @GetMapping("/{id}")
    public Cliente retornarCliente(@PathVariable int id)
    {
        return clienteService.retornarClientePorId(id);
    }

}
