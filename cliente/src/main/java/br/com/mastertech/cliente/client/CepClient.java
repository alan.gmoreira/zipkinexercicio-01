package br.com.mastertech.cliente.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "CEP")
@RequestMapping("/cep")
public interface CepClient {

    @GetMapping("/{cep}")
    Cep getCep(@PathVariable String cep);
}
